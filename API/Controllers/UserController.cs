using Application.Models;
using Application.UserApp;
using Domain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace API.Controllers
{
    public class UserController : BaseController
    {

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<ActionResult<UserModel>> Login(Login.Query query)
        {
          
            return await Mediator.Send(query);
        }
        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<ActionResult<UserModel>> Register(Register.Command command)
        {
            return await Mediator.Send(command);
        }

        [HttpGet]
        public async Task<ActionResult<UserModel>> CurrentUser()
        {
            return await Mediator.Send(new CurrentUser.Query());
        }

        [AllowAnonymous]
        [HttpPost("facebook")]
        public async Task<ActionResult<UserModel>> FacebookLogin(FacebookLogin.Query query)
        {
            return await Mediator.Send(query);
        }

        [HttpGet("service/{id}/check-order")]
        public async Task<ActionResult<bool>> CheckIfAnyOrders(Guid id)
        {
            return await Mediator.Send(new UserActiveOrderForService.Query { ServiceId = id });
        }




    }
}