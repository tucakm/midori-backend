using Application.Models;
using Application.Profiles;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace API.Controllers
{
    public class ProfilesController : BaseController
    {

        [AllowAnonymous]
        [HttpGet("{username}")]        
        public async Task<ActionResult<ProfileModel>> Get(string username)
        {
            return await Mediator.Send(new Details.Query { Username = username });
        }

        [HttpPut]
        public async Task<ActionResult<Unit>> EditProfile(EditProfile.Command command)
        {

            return await Mediator.Send(command);

        }

    }
}