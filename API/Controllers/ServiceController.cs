using Application.Models;
using Application.Reviews;
using Application.Services;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace API.Controllers
{
    public class ServiceController : BaseController
    {
        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<ServiceList.ServiceWrapper>> List(int? limit, int? offset,string criteria, string sort, ServiceStatus status)
        {
            return await Mediator.Send(new ServiceList.Query(limit, offset, criteria, sort, status));
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<ActionResult<ServiceModel>> Details(Guid id)
        {
            return await Mediator.Send(new ServiceDetails.Query { Id = id });
        }


        [HttpPut]
        public async Task<ActionResult<ServiceModel>> Create(ServiceSave.Command command)
        {
            var id = await Mediator.Send(command);
            return await Details(id);
        }
        [HttpPut("{id}/send-review")]
        public async Task<ActionResult<ReviewModel>> SendReview(Guid id, CreateReview.Command command)
        {
            command.ServiceId = id;
            return await Mediator.Send(command);
        }
        [HttpPatch("{id}/change-status")]
        public async Task<ActionResult<ServiceModel>> PublishService(Guid id, [FromBody] int? status)
        {

            var command = new ServiceChangeStatus.Command
            {
                Id = id,
                Status = status.Value
            };
            var serid = await Mediator.Send(command);
            return await Details(serid);
        }
        [HttpDelete("{id}")]
        [Authorize(Policy = "IsServiceOwner")]
        public async Task<ActionResult<Unit>> Delete(Guid id)
        {
            return await Mediator.Send(new ServiceDelete.Command { Id = id });
        }

        [AllowAnonymous]
        [HttpGet("profile/{username}")]
        public async Task<ActionResult<List<ServiceModel>>> ListServicesForUser(string username)
        {

            return await Mediator.Send(new GetServicesListForUser.Query { Username = username });
        }

        [AllowAnonymous]
        [HttpGet("tags")]
        public async Task<ActionResult<List<TagModel>>> ListAllTags()
        {
            return await Mediator.Send(new ListTags.Query());
        }
    }
}