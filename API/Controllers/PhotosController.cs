using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Application.Photos;
using System;

namespace API.Controllers
{
    public class PhotosController : BaseController
    {
        [HttpPost]
        public async Task<ActionResult<Photo>> Add([FromForm] PhotosAddAvatarPhoto.Command command)
        {
            return await Mediator.Send(command);
        }

        [HttpPost("service/{id:guid}")]
        public async Task<ActionResult<Photo>> AddServicePhoto([FromForm] PhotosAddServiceImage.Command command,Guid id)
        {
            command.Id=id;
            return await Mediator.Send(command);
        }




    }
}