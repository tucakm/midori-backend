﻿using Application.Models;
using Application.Notifications;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace API.Controllers
{
    public class NotificationController : BaseController
    {


        [HttpGet("{username}")]
        public async Task<ActionResult<List<NotificationModel>>> List(string username)
        {
            return await Mediator.Send(new GetNotificationsForUser.Query { Username = username });
        }
        [HttpPatch("{id}/toggleIsRead")]
        public async Task<ActionResult<Unit>> ToogleIsRed(Guid id, bool isRead)
        {
            return await Mediator.Send(new ToggleIsRead.Command
            {
                Id = id
            });
        }
    }
}
