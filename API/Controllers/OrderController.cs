﻿using Application.Models;
using Application.Orders;
using Application.Services;
using Domain;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace API.Controllers
{
    public class OrderController : BaseController
    {


        [HttpPut]
        public async Task<ActionResult<Guid>> CreateOrder(OrderSave.Command command)
        {

            return await Mediator.Send(command);

        }
        [HttpGet("{id}")]
        public async Task<ActionResult<OrderModel>> Details(Guid id)
        {
            return await Mediator.Send(new OrderDetails.Query { Id = id });
        }


        [HttpGet]
        public async Task<ActionResult<List<OrderModel>>> List(OrderStatus? orderStatus, DateTime? startDate, DateTime? endDate)
        {
            return await Mediator.Send(new OrderListForUser.Query(orderStatus, startDate, endDate));
        }

        [HttpGet("sent")]
        public async Task<ActionResult<List<OrderModel>>> ListSentOrders()
        {
            return await Mediator.Send(new OrdersSentList.Query());
        }
        [HttpPatch("{id}/change-status")]
        public async Task<ActionResult<OrderModel>> OrderChangeStatus(Guid id, [FromBody] OrderStatus status)
        {

            var command = new OrderChangeStatus.Command
            {
                Id = id,
                Status = status
            };
            var serid = await Mediator.Send(command);
            return await Details(serid);
        }
    }
}
