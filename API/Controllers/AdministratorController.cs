﻿using Application.Services;
using Domain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class AdministratorController:BaseController
    {
        [HttpGet]
        [Route("/admin/services")]
        public async Task<ActionResult<ServiceList.ServiceWrapper>> List(int? limit, int? offset,string criteria,ServiceStatus status,string sort)
        {
            return await Mediator.Send(new ServiceList.Query(limit, offset, criteria, sort, status));
        }
    }
}
