using Application.Reviews;
using MediatR;
using Microsoft.AspNetCore.SignalR;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace API.SignalR
{
    public class ChatHub : Hub
    {
        private readonly IMediator _mediator;
        public ChatHub(IMediator mediator)
        {
            this._mediator = mediator;
        }

        public async Task SendReview(CreateReview.Command command)
        {
            var username = GetUsername();

            command.Username = username;

            var review = await _mediator.Send(command);

            await Clients.Group(command.ServiceId.ToString()).SendAsync("ReceiveReview", review);
        }
        private string GetUsername()
        {
            return Context.User?.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
        }

        public async Task AddToGroup(string groupName)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, groupName);
            var username = GetUsername();
            await Clients.Group(groupName).SendAsync("Send", $"{username} has joined group");
        }
        public async Task RemoveFromGroup(string groupName)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, groupName);
            var username = GetUsername();
            await Clients.Group(groupName).SendAsync("Send", $"{username} has left group");
        }
    }
}