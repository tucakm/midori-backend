﻿using Application.Errors;
using Application.Models;
using AutoMapper;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistance;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Notifications
{
    public class GetNotificationsForUser
    {
        public class Query : IRequest<List<NotificationModel>>
        {
            public string Username { get; set; }
        }

        public class Handler : IRequestHandler<Query, List<NotificationModel>>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;

            public Handler(DataContext context, IMapper mapper)
            {
                this._mapper = mapper;
                _context = context;

            }

            public async Task<List<NotificationModel>> Handle(Query request,
             CancellationToken cancellationToken)
            {
                var user = await _context.Users.SingleOrDefaultAsync(x => x.UserName == request.Username);
                if (user == null)
                    throw new RestException(HttpStatusCode.NotFound, new { user = "NotFound" });

                var notifications = await _context.Notifications.Where(x => x.User.Id == user.Id && x.IsRead == false).ToListAsync();

                return _mapper.Map<List<Notification>, List<NotificationModel>>(notifications);

            }
        }

    }
}

