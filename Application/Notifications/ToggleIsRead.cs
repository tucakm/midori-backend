﻿using Application.Errors;
using FluentValidation;
using MediatR;
using Persistance;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Notifications
{
    public class ToggleIsRead
    {
        public class Command : IRequest<Unit>
        {
            public Guid Id { get; set; }
        }
        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.Id).NotEmpty();

            }
        }
        public class Handler : IRequestHandler<Command, Unit>
        {
            private readonly DataContext _context;
            public Handler(DataContext context)
            {

                _context = context;

            }
            public async Task<Unit> Handle(Command request,
                CancellationToken cancellationToken)
            {
                var notification = await _context.Notifications.FindAsync(request.Id);
                if (notification == null)
                    throw new RestException(HttpStatusCode.NotFound, new { order = "Not found" });

                notification.IsRead = true;

                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }

        }
    }
}
