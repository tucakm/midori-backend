using Application.Models;
using Domain;

namespace Application
{
    public class MappingProfile : AutoMapper.Profile
    {
        public MappingProfile()
        {


            CreateMap<Service, ServiceModel>()
                .ForMember(p => p.Image, o => o.MapFrom(s => s.Photo.Url));

            CreateMap<Review, ReviewModel>();

            CreateMap<AppUser, UserModel>()
                .ForMember(p => p.Image, o => o.MapFrom(s => s.Photo.Url));

            CreateMap<AppUser, ProfileModel>()
                .ForMember(p => p.Image, o => o.MapFrom(s => s.Photo.Url));

            CreateMap<ServiceTags, TagModel>()
                .ForMember(p => p.TagId, o => o.MapFrom(s => s.TagId))
                .ForMember(p => p.TagName, o => o.MapFrom(s => s.Tag.Name));

            CreateMap<Tag, TagModel>()
             .ForMember(p => p.TagId, o => o.MapFrom(t => t.Id))
             .ForMember(p => p.TagName, o => o.MapFrom(t => t.Name));

            CreateMap<Role, RoleModel>();

            CreateMap<Order, OrderModel>();

            CreateMap<Notification, NotificationModel>();
        }

    }
}