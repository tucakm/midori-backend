using Application.Models;
using AutoMapper;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistance;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace Application.Services
{
    public class ServiceList
    {
        public class ServiceWrapper
        {
            public List<ServiceModel> Services { get; set; }
            public int ServiceCount { get; set; }
        }
        public class Query : IRequest<ServiceWrapper>
        {
            public int? Limit { get; set; }
            public int? Offset { get; set; }
            public ServiceStatus? Status { get; set; }
            public string Criteria { get; set; }
            public string Sort { get; set; }

            public Query(int? limit, int? offset,string criteria, string sort, ServiceStatus? status = ServiceStatus.Active)
            {
                Limit = limit;
                Status = status;
                Offset = offset;
                Sort = sort;
                Criteria = criteria;
             }       

        }

        public class Handler : IRequestHandler<Query, ServiceWrapper>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;

            public Handler(DataContext context, IMapper mapper)
            {
                this._mapper = mapper;
                _context = context;

            }

            public async Task<ServiceWrapper> Handle(Query request,
             CancellationToken cancellationToken)
            {
                var querable = _context.Services
                   .AsQueryable();
                if (request.Status != null)
                    querable = querable.Where(x => x.Status == request.Status.Value);
                if (request.Criteria != null)
                {
                    querable = querable.Where(x => x.Name.ToLower().Trim().Contains(request.Criteria.ToLower().Trim())||  x.ServiceTags.Any(x => x.Tag.Name.ToLower().Trim().Contains(request.Criteria.ToLower().Trim())));
                                    
                }
                if (request.Sort != null)
                     querable = request.Sort=="ASC"? querable.OrderBy(x => x.Price): querable.OrderByDescending(x => x.Price);

                var services = 
                    await querable
                    .Skip(request.Offset ?? 0)
                    .Take(request.Limit ?? 8)
                    .ToListAsync();
                var serviceList = _mapper.Map<List<Service>, List<ServiceModel>>(services);

                foreach (var serviceItem in serviceList)
                {
                    if (serviceItem.Review.Count > 0)
                        serviceItem.Rating = serviceItem.Review.Sum(x => x.Rate) / serviceItem.Review.Count;
                }

                return new ServiceWrapper {
                    Services = serviceList,
                    ServiceCount = querable.Count()
                };

            }
        }
    }
}