using Application.Errors;
using Application.Interfaces;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistance;
using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Services
{
    public class ServiceDelete
    {
        public class Command : IRequest
        {
            public Guid Id { get; set; }
        }
        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            public Handler(DataContext context, IUserAccessor userAccessor)
            {
                _context = context;

            }
            public async Task<Unit> Handle(Command request,
            CancellationToken cancellationToken)
            {
                var service = await _context.Services.FindAsync(request.Id);

                if (service == null)
                    throw new RestException(HttpStatusCode.NotFound, new { service = "Not found" });

                var reviews = await _context.Reviews.Where(x => x.Service.Id == service.Id).ToListAsync();
                foreach (Review review in reviews)
                {
                    _context.Remove(review);
                }

                _context.Remove(service);
                var succes = await _context.SaveChangesAsync() > 0;

                if (succes) return Unit.Value;

                throw new Exception("Problem saving changes");


            }
        }

    }
}