using Application.Models;
using AutoMapper;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistance;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Services
{
    public class ListTags
    {
        public class Query : IRequest<List<TagModel>> { }

        public class Handler : IRequestHandler<Query, List<TagModel>>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;

            public Handler(DataContext context, IMapper mapper)
            {
                this._mapper = mapper;
                _context = context;

            }

            public async Task<List<TagModel>> Handle(Query request,
             CancellationToken cancellationToken)
            {

                var tags = await _context.Tag.Distinct().ToListAsync();

                return _mapper.Map<List<Tag>, List<TagModel>>(tags);

            }
        }
    }
}