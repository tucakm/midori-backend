using Application.Errors;
using Application.Models;
using AutoMapper;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistance;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Services
{
    public class GetServicesListForUser
    {
        public class Query : IRequest<List<ServiceModel>>
        {
            public string Username { get; set; }
        }

        public class Handler : IRequestHandler<Query, List<ServiceModel>>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;

            public Handler(DataContext context, IMapper mapper)
            {
                this._mapper = mapper;
                _context = context;

            }

            public async Task<List<ServiceModel>> Handle(Query request,
             CancellationToken cancellationToken)
            {
                var user = await _context.Users.SingleOrDefaultAsync(x => x.UserName == request.Username);
                if (user == null)
                    throw new RestException(HttpStatusCode.NotFound, new { user = "NotFound" });

                var services = await _context.Services.Where(x => x.AppUserId == user.Id).ToListAsync();
                var serviceList = _mapper.Map<List<Service>, List<ServiceModel>>(services);
                foreach (var serviceItem in serviceList)
                {
                    if (serviceItem.Review.Count > 0)
                        serviceItem.Rating = serviceItem.Review.Sum(x => x.Rate) / serviceItem.Review.Count;
                }


                return serviceList;

            }
        }

    }
}