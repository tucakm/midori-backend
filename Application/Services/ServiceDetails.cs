using Application.Errors;
using Application.Models;
using AutoMapper;
using Domain;
using MediatR;
using Persistance;
using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Services
{
    public class ServiceDetails
    {
        public class Query : IRequest<ServiceModel>
        {
            public Guid Id { get; set; }
        }

        public class Handler : IRequestHandler<Query, ServiceModel>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;

            public Handler(DataContext context, IMapper mapper)
            {
                this._mapper = mapper;
                _context = context;

            }

            public async Task<ServiceModel> Handle(Query request,
             CancellationToken cancellationToken)
            {
                var service = await _context.Services.FindAsync(request.Id);

                if (service == null)
                    throw new RestException(HttpStatusCode.NotFound, new { service = "Not found" });

                var serviceModel = _mapper.Map<Service, ServiceModel>(service);
                if (serviceModel.Review.Count > 0)
                    serviceModel.Rating = serviceModel.Review.Sum(x => x.Rate) / serviceModel.Review.Count;

                return serviceModel;
            }
        }
    }
}