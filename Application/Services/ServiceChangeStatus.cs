using Application.Errors;
using Application.Interfaces;
using Domain;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistance;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Services
{
    public class ServiceChangeStatus
    {
        public class Command : IRequest<Guid>
        {
            public Guid Id { get; set; }
            public int Status { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.Id).NotEmpty();
                RuleFor(x => x.Status).NotEmpty();
            }
        }
        public class Handler : IRequestHandler<Command, Guid>
        {
            private readonly DataContext _context;
            private readonly IUserAccessor _userAccessor;
            public Handler(DataContext context, IUserAccessor userAccessor)
            {

                this._userAccessor = userAccessor;
                _context = context;

            }
            public async Task<Guid> Handle(Command request,
            CancellationToken cancellationToken)
            {
                var user = await _context.Users.SingleOrDefaultAsync(x => x.UserName == _userAccessor.GetCurrentUsername());

                var service = await _context.Services.FindAsync(request.Id);
                if(service == null)
                    throw new RestException(HttpStatusCode.NotFound, new { service = "Not found" });
                
                service.Status =request.Status == 2 ? ServiceStatus.Active : ServiceStatus.Inactive;           
                await _context.SaveChangesAsync(cancellationToken);


                return service.Id;

            }   
         }

    }
}