using Application.Errors;
using Application.Interfaces;
using Domain;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistance;
using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Services
{
    public class ServiceSave
    {
        public class Command : IRequest<Guid>
        {
            public Guid? Id { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public string Introduction { get; set; }
            public decimal Price { get; set; }
            public string[] Tags { get; set; }
            public ServiceStatus? Status { get; set; }
            public string City { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.Name).NotEmpty();
                RuleFor(x => x.Description).NotEmpty();
                RuleFor(x => x.Introduction).NotEmpty();
                RuleFor(x => x.Price).NotEmpty();
            }
        }
        public class Handler : IRequestHandler<Command, Guid>
        {
            private readonly DataContext _context;
            private readonly IUserAccessor _userAccessor;
            public Handler(DataContext context, IUserAccessor userAccessor)
            {

                this._userAccessor = userAccessor;
                _context = context;

            }
            public async Task<Guid> Handle(Command request,
            CancellationToken cancellationToken)
            {
                var user = await _context.Users.SingleOrDefaultAsync(x => x.UserName == _userAccessor.GetCurrentUsername());

                Service service = request.Id != null ?
                     HandeEdit(request, await _context.Services.FindAsync(request.Id)) :
                     HandeSaveNew(request, user);


                if (request.Id == null) _context.Services.Add(service);

                if (request.Tags != null || request.Tags.Length > 0)
                {
                    await HandleTags(request.Tags, service.Id);
                }
                await _context.SaveChangesAsync(cancellationToken);


                return service.Id;

                //throw new Exception("Problem saving changes");

            }
            private async Task HandleTags(String[] tags, Guid serviceId)
            {
                if (tags == null || tags.Length == 0)
                    return;

                var listOfTags = await _context.ServiceTags.Where(x => x.ServiceId == serviceId).ToListAsync();
              
                foreach (var tag in tags)
                {
                    var item = listOfTags.Find(x => x.Tag.Name.Trim() == tag.Trim());
                    if (item != null)
                    {
                        listOfTags.Remove(item);
                        break;
                    }
                    var existingTag = await _context.Tag.FirstOrDefaultAsync(x => x.Name.Trim() == tag.Trim());
                    if (existingTag == null)
                    {
                        var newTag = new Tag
                        {
                            Id = Guid.NewGuid(),
                            Name = tag,
                        };
                        existingTag = newTag;
                       _context.Tag.Add(newTag);
                    }
                                  
                    _context.ServiceTags.Add(new ServiceTags
                    {
                        ServiceId = serviceId,
                        TagId = existingTag.Id
                    });
                }
                foreach (var remTag in listOfTags)
                {
                    _context.ServiceTags.Remove(remTag);                 
                }

            }

            private Service HandeSaveNew(Command request, AppUser user)
            {
                return new Service
                {
                    Id = Guid.NewGuid(),
                    Name = request.Name,
                    Description = request.Description,
                    Introduction = request.Introduction,
                    Price = request.Price,
                    CreatedDate = DateTime.Now,
                    User = user,
                    Status = ServiceStatus.Draft,
                    City = request.City
                };
            }
            private Service HandeEdit(Command request, Service service)
            {


                if (service == null)
                    throw new RestException(HttpStatusCode.NotFound, new { service = "NotFound" });

                service.Name = request.Name ?? service.Name;
                service.Description = request.Description ?? service.Description;
                service.Introduction = request.Introduction ?? service.Introduction;
                service.Price = request.Price == 0 ? service.Price : request.Price;
                service.Status = request.Status ?? service.Status;
                service.City = request.City ?? service.City;
                return service;
            }


        }

    }
}