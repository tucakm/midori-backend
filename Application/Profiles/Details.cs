using Application.Errors;
using Application.Models;
using AutoMapper;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistance;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Profiles
{
    public class Details
    {
        public class Query : IRequest<ProfileModel>
        {

            public string Username { get; set; }

        }

        public class Handler : IRequestHandler<Query, ProfileModel>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;

            public Handler(DataContext context, IMapper mapper)
            {
                this._mapper = mapper;
                _context = context;

            }

            public async Task<ProfileModel> Handle(Query request,
             CancellationToken cancellationToken)
            {
                var user = await _context.Users.SingleOrDefaultAsync(x => x.UserName == request.Username, cancellationToken);

                if (user == null)
                    throw new RestException(HttpStatusCode.NotFound, new { user = "NotFound" });

                return _mapper.Map<AppUser, ProfileModel>(user);

            }
        }
    }
}