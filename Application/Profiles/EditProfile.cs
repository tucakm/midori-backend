using Application.Errors;
using Application.Interfaces;
using Domain;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Persistance;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Profiles
{
    public class EditProfile
    {

        public class Command : IRequest
        {

            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Bio { get; set; }
            public string Address { get; set; }
            public string PhoneNumber { get; set; }
            public string Job { get; set; }
            public string Website { get; set; }
            public string City { get; set; }
            public string Company { get; set; }
            public string ZipCode { get; set; }
            public string Country { get; set; }
            public string AboutMe { get; set; }

            public string FaceBookLink { get; set; }
            public string TwitterLink { get; set; }
            public string InstagramLink { get; set; }

        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {

            }
        }
        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            private readonly UserManager<AppUser> _userManager;
            private readonly IUserAccessor _userAccessor;
            public Handler(DataContext context, UserManager<AppUser> userManager,

            IJwtGenerator jwtGenerator, IUserAccessor userAccessor)
            {
                this._userAccessor = userAccessor;

                this._userManager = userManager;
                _context = context;

            }
            public async Task<Unit> Handle(Command request,
            CancellationToken cancellationToken)
            {
                var user = await _context.Users.SingleOrDefaultAsync(x => x.UserName == _userAccessor.GetCurrentUsername());

                if (user == null)
                    throw new RestException(HttpStatusCode.NotFound, new { user = "User not found" });

                user.FirstName = request.FirstName ?? user.FirstName;
                user.LastName = request.LastName ?? user.LastName;
                user.Bio = request.Bio ?? user.Bio;
                user.Address = request.Address ?? user.Address;
                user.PhoneNumber = request.PhoneNumber ?? user.PhoneNumber;
                user.Job = request.Job ?? user.Job;
                user.Website = request.Website ?? user.Website;
                user.City = request.City ?? user.City;
                user.Company = request.Company ?? user.Company;
                user.ZipCode = request.ZipCode ?? user.ZipCode;
                user.Country = request.Country ?? user.Country;
                user.AboutMe = request.AboutMe ?? user.AboutMe;
                user.FaceBookLink = request.FaceBookLink ?? user.FaceBookLink;
                user.TwitterLink = request.TwitterLink ?? user.TwitterLink;
                user.InstagramLink = request.InstagramLink ?? user.InstagramLink;

                var succes = await _context.SaveChangesAsync() > 0;

                return Unit.Value;



            }
        }


    }
}