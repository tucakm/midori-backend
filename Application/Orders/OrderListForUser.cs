﻿using Application.Interfaces;
using Application.Models;
using AutoMapper;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Orders
{

    public class OrderListForUser
    {

        public class Query : IRequest<List<OrderModel>>
        {
            public Guid? ServiceId { get; set; }
            public OrderStatus? OrderStatus { get; set; }

            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }

            public Query(OrderStatus? orderStatus, DateTime? startDate, DateTime? endDate)
            {

                OrderStatus = orderStatus;
                StartDate = startDate;
                EndDate = endDate;
            }

        }
        public class Handler : IRequestHandler<Query, List<OrderModel>>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;
            private readonly IUserAccessor _userAccessor;
            public Handler(DataContext context, IMapper mapper, IUserAccessor userAccessor)
            {
                _mapper = mapper;
                _context = context;
                _userAccessor = userAccessor;

            }

            public async Task<List<OrderModel>> Handle(Query request, CancellationToken cancellationToken)
            {
                var user = await _context.Users.SingleOrDefaultAsync(x => x.UserName == _userAccessor.GetCurrentUsername());

                var querable = _context.Orders.AsQueryable().Where(x => x.Service.User.UserName == user.UserName);

                if (request.OrderStatus != null)
                    querable = querable.Where(x => x.Status == request.OrderStatus.Value);
                else
                    querable = querable.Where(x => (x.Status != OrderStatus.Declined) && (x.Status != OrderStatus.Finished));
                if (request.ServiceId != null)
                    querable = querable.Where(x => x.ServiceId == request.ServiceId.Value);
                if (request.StartDate != null)
                    querable = querable.Where(x => x.Date >= request.StartDate);
                if (request.EndDate != null)
                    querable = querable.Where(x => x.Date <= request.EndDate);

                var orders = await querable.ToListAsync();

                return _mapper.Map<List<Order>, List<OrderModel>>(orders);
            }
        }
    }
}
