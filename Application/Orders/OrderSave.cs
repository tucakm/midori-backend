using Application.Errors;
using Application.Interfaces;
using Domain;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistance;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Orders
{
    public class OrderSave
    {
        public class Command : IRequest<Guid>
        {
            public Guid? Id { get; set; }
            public Service Service { get; set; }
            public DateTime Date { get; set; }
            public DateTime StartTime { get; set; }
            public DateTime EndTime { get; set; }
            public string Comment { get; set; }
            public OrderStatus? status { get; set; }

        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.Service).NotEmpty();
                RuleFor(x => x.Date).NotEmpty();
                RuleFor(x => x.StartTime).NotEmpty();
                RuleFor(x => x.EndTime).NotEmpty();

            }
        }
        public class Handler : IRequestHandler<Command, Guid>
        {
            private readonly DataContext _context;
            private readonly IUserAccessor _userAccessor;
            public Handler(DataContext context, IUserAccessor userAccessor)
            {

                this._userAccessor = userAccessor;
                _context = context;

            }
            public async Task<Guid> Handle(Command request,
            CancellationToken cancellationToken)
            {
                var user = await _context.Users.SingleOrDefaultAsync(x => x.UserName == _userAccessor.GetCurrentUsername());

                var service = await _context.Services.SingleOrDefaultAsync(x => x.Id == request.Service.Id);

                if (service == null)
                    throw new RestException(System.Net.HttpStatusCode.NotFound, new { Service = "Not found " });

                var order = request.Id == null ? HandleSave(user, request) : HandleEdit(request, await _context.Orders.FindAsync(request.Id));

                if (request.Id == null)
                {
                    var orderds =
                          await _context.Orders.Where(x => (x.StartTime <= request.StartTime && request.StartTime <= x.EndTime) ||
                          (x.StartTime <= request.EndTime && request.EndTime <= x.EndTime))
                          .Where(x => x.Status == OrderStatus.Aproved)
                          .Where(x => x.Service.User == service.User).ToListAsync();

                    if (orderds != null && orderds.Count > 0) order.Status = OrderStatus.Reschedule;
                    _context.Orders.Add(order);

                }

                if (request.status != null && request.status == OrderStatus.RescheduleSent)
                {
                    var notifications = new Notification
                    {
                        OrderedService = order.Service,
                        Order = order,
                        User = order.User,
                        Type = NotificationType.RescheduleOrder,
                        Message = $"Reschedule is asked, are you ok with Date " +
                        $"{order.Date.ToString("MM/dd/yyyy")} from {order.StartTime.TimeOfDay} to {order.EndTime.TimeOfDay}"


                    };
                    _context.Notifications.Add(notifications);
                }
                else
                {
                    var notifications = new Notification
                    {
                        OrderedService = order.Service,
                        User = service.User,
                        Type = NotificationType.NewOrder,
                        Order = order,
                        Message = $"New order from {user.UserName} has arrived, check callendar"
                    };
                    _context.Notifications.Add(notifications);

                }

                var succes = await _context.SaveChangesAsync(cancellationToken) > 0;
                if (succes) return order.Id;

                throw new Exception("Problem saving changes");

            }
            private Order HandleSave(AppUser user, Command request)
            {
                return new Order
                {
                    Id = Guid.NewGuid(),
                    ServiceId = request.Service.Id,
                    AppUserId = user.Id,
                    Date = request.Date,
                    StartTime = request.StartTime,
                    Status = OrderStatus.Pending,
                    EndTime = request.EndTime,
                    TotalPrice = (request.EndTime.Hour - request.StartTime.Hour) * request.Service.Price,
                    Comment = request.Comment
                };
            }
            private Order HandleEdit(Command request, Order order)
            {
                order.Date = request.Date != null ? request.Date : order.Date;
                order.StartTime = request.StartTime != null ? request.StartTime : order.StartTime;
                order.EndTime = request.EndTime != null ? request.EndTime : order.EndTime;
                order.Status = request.status ?? order.Status;

                return order;
            }
        }

    }
}