﻿using Application.Interfaces;
using Application.Models;
using AutoMapper;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistance;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Orders
{
    public class OrdersSentList
    {
        public class Query : IRequest<List<OrderModel>>
        {

        }
        public class Handler : IRequestHandler<Query, List<OrderModel>>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;
            private readonly IUserAccessor _userAccessor;
            public Handler(DataContext context, IMapper mapper, IUserAccessor userAccessor)
            {
                _mapper = mapper;
                _context = context;
                _userAccessor = userAccessor;

            }

            public async Task<List<OrderModel>> Handle(Query request, CancellationToken cancellationToken)
            {
                var user = await _context.Users.SingleOrDefaultAsync(x => x.UserName == _userAccessor.GetCurrentUsername());

                var querable = _context.Orders.AsQueryable().Where(x => x.User.UserName == user.UserName);

                var orders = await querable.OrderByDescending(x => x.EndTime).ToListAsync();

                return _mapper.Map<List<Order>, List<OrderModel>>(orders);

            }
        }
    }
}
