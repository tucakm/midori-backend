﻿using Application.Errors;
using Application.Models;
using AutoMapper;
using Domain;
using MediatR;
using Persistance;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Orders
{
    public class OrderDetails
    {
        public class Query : IRequest<OrderModel>
        {
            public Guid Id { get; set; }
        }

        public class Handler : IRequestHandler<Query, OrderModel>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;

            public Handler(DataContext context, IMapper mapper)
            {
                this._mapper = mapper;
                _context = context;

            }

            public async Task<OrderModel> Handle(Query request,
             CancellationToken cancellationToken)
            {
                var order = await _context.Orders.FindAsync(request.Id);

                if (order == null)
                    throw new RestException(HttpStatusCode.NotFound, new { order = "Not found" });

                var orderModel = _mapper.Map<Order, OrderModel>(order);

                return orderModel;
            }
        }
    }
}
