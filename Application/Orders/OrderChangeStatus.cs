using Application.Errors;
using Application.Interfaces;
using Domain;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistance;
using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Services
{
    public class OrderChangeStatus
    {
        public class Command : IRequest<Guid>
        {
            public Guid Id { get; set; }
            public OrderStatus Status { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.Id).NotEmpty();
                RuleFor(x => x.Status).NotEmpty();
            }
        }
        public class Handler : IRequestHandler<Command, Guid>
        {
            private readonly DataContext _context;
            public Handler(DataContext context, IUserAccessor userAccessor)
            {

                _context = context;

            }
            public async Task<Guid> Handle(Command request,
            CancellationToken cancellationToken)
            {

                var order = await _context.Orders.FindAsync(request.Id);
                if (order == null)
                    throw new RestException(HttpStatusCode.NotFound, new { order = "Not found" });

                if (request.Status == OrderStatus.Aproved)
                {
                    order.Status = request.Status;
                    var orderds =
                         await _context.Orders
                         .Where(x => (x.StartTime <= order.StartTime && order.StartTime <= x.EndTime) ||
                          (x.StartTime <= order.EndTime && order.EndTime <= x.EndTime))
                         .Where(x => x.Status == OrderStatus.Pending)
                         .Where(x => x.Service.User == order.Service.User)
                         .Where(x=>x.Id!=request.Id).ToListAsync();
                              
                    foreach (var orderItem in orderds)
                    {
                        orderItem.Status = OrderStatus.Reschedule;
                    }

                }
                if (request.Status == OrderStatus.Finished)
                {
                    var notification = new Notification
                    {
                        OrderedService = order.Service,
                        User = order.User,
                        Order = order,
                        Message = "Your service has been finished, please leave review",
                        Type = NotificationType.Review,
                    };
                    _context.Notifications.Add(notification);
                }
                order.Status = request.Status;
                
                if(request.Status == OrderStatus.Aproved || request.Status == OrderStatus.Declined)
                {
                    var notification = _context.Notifications.Where(x => x.Order.Id == order.Id && !x.IsRead && x.Type == NotificationType.RescheduleOrder).FirstOrDefault();

                    if(notification!=null) notification.IsRead = true;

                }
                await _context.SaveChangesAsync(cancellationToken);


                return order.Id;

            }

        }

    }
}