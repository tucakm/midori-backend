using System;

namespace Application.Models
{
    public class TagModel
    {
        public Guid TagId { get; set; }
        public string TagName { get; set; }
    }
}