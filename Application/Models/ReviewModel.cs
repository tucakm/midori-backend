using System;

namespace Application.Models
{
    public class ReviewModel
    {
        public Guid Id { get; set; }
        public string Text { get; set; }
        public int Rate { get; set; }
        public DateTime Date { get; set; }
        public UserModel User { get; set; }

    }
}