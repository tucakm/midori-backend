﻿using Domain;
using System;

namespace Application.Models
{
    public class NotificationModel
    {
        public Guid Id { get; set; }

        public ServiceModel OrderedService { get; set; }

        public OrderModel order { get; set; }

        public UserModel User { get; set; }

        public string Message { get; set; }

        public DateTime Date { get; set; }

        public bool IsRead { get; set; }

        public NotificationType Type { get; set; }
    }
}
