﻿using Domain;
using System;

namespace Application.Models
{
    public class OrderModel
    {
        public Guid Id { get; set; }
        public ServiceModel Service { get; set; }
        public UserModel User { get; set; }
        public DateTime Date { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public OrderStatus Status { get; set; }
        public decimal TotalPrice { get; set; }
        public string Comment { get; set; }
    }
}
