
using Domain;
using System;
using System.Collections.Generic;

namespace Application.Models
{
    public class ServiceModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Introduction { get; set; }
        public decimal Price { get; set; }
        public DateTime CreatedDate { get; set; }
        public UserModel User { get; set; }
        public ICollection<ReviewModel> Review { get; set; }
        public string Image { get; set; }
        public decimal Rating { get; set; }
        public ICollection<TagModel> ServiceTags { get; set; }
        public ServiceStatus Status { get; set; }
        public string City { get; set; }
    }
}