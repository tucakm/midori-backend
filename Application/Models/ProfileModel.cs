
using Domain;
using System;
using System.Collections.Generic;

namespace Application.Models
{
    public class ProfileModel
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Bio { get; set; }
        public string Address { get; set; }
        public DateTime Birthday { get; set; }
        public string PhoneNumber { get; set; }
        public string Job { get; set; }
        public string Website { get; set; }
        public string City { get; set; }
        public string Company { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public string AboutMe { get; set; }

        public string Image { get; set; }
        public Photo Photo { get; set; }
        public string FaceBookLink { get; set; }
        public string TwitterLink { get; set; }
        public string InstagramLink { get; set; }
        public ICollection<ServiceModel> Service { get; set; }
    }
}