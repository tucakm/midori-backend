using Application.Interfaces;
using Domain;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Persistance;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Photos
{
    public class PhotosAddServiceImage
    {
        public class Command : IRequest<Photo>
        {
            public Guid Id { get; set; }
            public IFormFile File { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {


        }
        public class Handler : IRequestHandler<Command, Photo>
        {
            private readonly DataContext _context;
            private readonly IPhotoAccessor _photoAccessor;
            public Handler(DataContext context, IPhotoAccessor photoAccessor)
            {
                this._photoAccessor = photoAccessor;
                _context = context;

            }
            public async Task<Photo> Handle(Command request,
            CancellationToken cancellationToken)
            {
                var photoUploadResult = _photoAccessor.AddPhoto(request.File);

                var service = await _context.Services.SingleOrDefaultAsync(x => x.Id == request.Id);

                var photo = new Photo
                {
                    Url = photoUploadResult.Url,
                    Id = photoUploadResult.PublicId
                };

                if (service.Photo != null)
                {
                    var oldPhoto = service.Photo;
                    var result = _photoAccessor.DeletePhoto(oldPhoto.Id);
                    _context.Photos.Remove(oldPhoto);
                }
                service.Photo = photo;
                var succes = await _context.SaveChangesAsync() > 0;

                if (succes) return photo;

                throw new Exception("Problem saving changes");


            }
        }
    }
}