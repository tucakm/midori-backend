using System;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Persistance;

namespace Application.Photos
{
    public class PhotosAddAvatarPhoto
    {
        public class Command : IRequest<Photo>
        {
            public IFormFile File { get; set; }
        }
        public class Handler : IRequestHandler<Command, Photo>
        {
            private readonly DataContext _context;
            private readonly IUserAccessor _userAccessor;

            private readonly IPhotoAccessor _photoAccessor;
            public Handler(DataContext context, IUserAccessor userAccessor, IPhotoAccessor photoAccessor)
            {
                this._userAccessor = userAccessor;
                this._photoAccessor = photoAccessor;

                _context = context;

            }
            public async Task<Photo> Handle(Command request,
            CancellationToken cancellationToken)
            {
                var photoUploadResult = _photoAccessor.AddPhoto(request.File);

                var user = await _context.Users.SingleOrDefaultAsync(x => x.UserName == _userAccessor.GetCurrentUsername());

                var photo = new Photo
                {
                    Url = photoUploadResult.Url,
                    Id = photoUploadResult.PublicId
                };

                if (user.Photo != null)
                {
                    var oldPhoto=user.Photo;
                    var result = _photoAccessor.DeletePhoto(oldPhoto.Id);
                    _context.Photos.Remove(oldPhoto);
                }
                user.Photo=photo;
                var succes = await _context.SaveChangesAsync() > 0;

                if (succes) return photo;

                throw new Exception("Problem saving changes");


            }
        }

    }
}