using Application.Errors;
using Application.Interfaces;
using Application.Models;
using AutoMapper;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistance;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Reviews
{
    public class CreateReview
    {
        public class Command : IRequest<ReviewModel>
        {
            public string Text { get; set; }
            public Guid ServiceId { get; set; }
            public string Username { get; set; }
            public int Rate { get; set; }
        }
        public class Handler : IRequestHandler<Command, ReviewModel>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;
            private readonly IUserAccessor _userAccessor;
            public Handler(DataContext context, IMapper mapper, IUserAccessor userAccessor)
            {
                this._mapper = mapper;
                _context = context;
                this._userAccessor = userAccessor;

            }
            public async Task<ReviewModel> Handle(Command request,
            CancellationToken cancellationToken)
            {

                var user = await _context.Users.SingleOrDefaultAsync(x => x.UserName == _userAccessor.GetCurrentUsername());

                var service = await _context.Services.FindAsync(request.ServiceId);

                if (service == null)
                    throw new RestException(HttpStatusCode.NotFound, new { Service = "Not found" });


                var review = new Review
                {
                    User = user,
                    Service = service,
                    Text = request.Text,
                    Rate = request.Rate,
                    Date = DateTime.Now
                };
                service.Review.Add(review);
                var succes = await _context.SaveChangesAsync() > 0;


                if (succes) return _mapper.Map<ReviewModel>(review);

                throw new Exception("Problem saving changes");


            }
        }

    }
}