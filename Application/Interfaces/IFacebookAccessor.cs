using System.Threading.Tasks;
using Application.UserApp.Dtos;

namespace Application.Interfaces
{
    public interface IFacebookAccessor
    {
         Task<FacebookUserInfo> FacebookLogin(string accessToken);
    }
}