﻿using Application.Interfaces;
using Application.Models;
using AutoMapper;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistance;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.UserApp
{
    public class UserActiveOrderForService
    {
        public class Query : IRequest<bool>
        {
            public Guid ServiceId { get; set; }
        }

        public class Handler : IRequestHandler<Query, bool>
        {
            private readonly DataContext _context;
            private readonly IUserAccessor _userAccessor;
            public Handler(DataContext context, IUserAccessor userAccessor)
            {
                this._userAccessor = userAccessor;
                _context = context;

            }

            public async Task<bool> Handle(Query request,
             CancellationToken cancellationToken)
            {
                var user = await _context.Users.SingleOrDefaultAsync(x => x.UserName == _userAccessor.GetCurrentUsername());

                var orders = await _context.Orders.Where(x => (x.User.Id == user.Id) && (x.ServiceId == request.ServiceId) && (x.Status == OrderStatus.Reschedule || x.Status == OrderStatus.Aproved || x.Status==OrderStatus.Pending || x.Status ==OrderStatus.RescheduleSent)).ToListAsync();

                if (orders != null && orders.Count>0)
                    return true;
                else
                    return false;
            }
        }
    }
}
