using Application.Errors;
using Application.Interfaces;
using Application.Models;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Application.UserApp
{
    public class Login
    {
        public class Query : IRequest<UserModel>
        {
            public string Email { get; set; }
            public string Password { get; set; }
        }
        public class QueryValidator : AbstractValidator<Query>
        {
            public QueryValidator()
            {
                RuleFor(x => x.Email).NotEmpty();
                RuleFor(x => x.Password).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<Query, UserModel>
        {
            private readonly UserManager<Domain.AppUser> _userManager;
            private readonly SignInManager<Domain.AppUser> _signInManager;
            private readonly IJwtGenerator _jwtGenerator;

            public Handler(UserManager<Domain.AppUser> userManager, SignInManager<Domain.AppUser> signInManager, IJwtGenerator jwtGenerator)
            {
                this._jwtGenerator = jwtGenerator;
                this._signInManager = signInManager;
                this._userManager = userManager;
            }

            public async Task<UserModel> Handle(Query request,
             CancellationToken cancellationToken)
            {
                var user = await _userManager.FindByEmailAsync(request.Email);
                if (user == null)
                    throw new RestException(HttpStatusCode.Unauthorized);

                var result = await _signInManager.CheckPasswordSignInAsync(user, request.Password, false);

                if (result.Succeeded)
                {
                    return new UserModel
                    {
                        Username = user.UserName,
                        Token = _jwtGenerator.CreateToken(user),
                        Email = user.Email,
                        Image = user.Photo?.Url
                    };
                }
                throw new RestException(HttpStatusCode.Unauthorized);


            }
        }

    }
}