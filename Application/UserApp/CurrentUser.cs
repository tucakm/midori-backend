using Application.Interfaces;
using Application.Models;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Persistance;
using System.Threading;
using System.Threading.Tasks;

namespace Application.UserApp
{
    public class CurrentUser
    {
        public class Query : IRequest<UserModel>
        {

        }

        public class Handler : IRequestHandler<Query, UserModel>
        {
            private readonly DataContext _context;
            private readonly UserManager<AppUser> _userManager;
            private readonly IJwtGenerator _jwtGenerator;
            private readonly IUserAccessor _userAccessor;

            public Handler(DataContext context, UserManager<AppUser> userManager,
            IJwtGenerator jwtGenerator, IUserAccessor userAccessor)
            {
                this._userAccessor = userAccessor;
                this._jwtGenerator = jwtGenerator;
                this._userManager = userManager;
                _context = context;

            }

            public async Task<UserModel> Handle(Query request,
             CancellationToken cancellationToken)
            {
                var user = await _userManager.FindByNameAsync(_userAccessor.GetCurrentUsername());

                return new UserModel
                {
                    Username = user.UserName,
                    Email = user.Email,
                    Token = _jwtGenerator.CreateToken(user),
                    Image = user.Photo?.Url

                };

            }
        }
    }
}