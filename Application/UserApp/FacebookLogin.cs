using Application.Errors;
using Application.Interfaces;
using Application.Models;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Persistance;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Application.UserApp
{
    public class FacebookLogin
    {
        public class Query : IRequest<UserModel>
        {

            public string AccessToken { get; set; }
        }

        public class Handler : IRequestHandler<Query, UserModel>
        {
            private readonly DataContext _context;

            private readonly UserManager<AppUser> _userManager;
            private readonly IFacebookAccessor _facebookAccessor;
            private readonly IJwtGenerator _jwtGenerator;

            public Handler(DataContext context, UserManager<AppUser> userManager, IFacebookAccessor facebookAccessor, IJwtGenerator jwtGenerator)
            {
                this._jwtGenerator = jwtGenerator;
                this._facebookAccessor = facebookAccessor;
                this._userManager = userManager;

                _context = context;

            }

            public async Task<UserModel> Handle(Query request,
             CancellationToken cancellationToken)
            {
                var userInfo = await _facebookAccessor.FacebookLogin(request.AccessToken);

                if (userInfo == null)
                    throw new RestException(HttpStatusCode.BadRequest, new { User = "Problem validating token" });
                var user = await _userManager.FindByEmailAsync(userInfo.Email);

                if (user == null)
                {
                    user = new AppUser
                    {
                        FirstName = userInfo.Name,
                        UserName = "fb_" + userInfo.Id,
                        Id = userInfo.Id,
                        Email = userInfo.Email

                    };
                    //To DO when adding photo
                    var photo = new Photo
                    {
                        Id = "fb_" + userInfo.Id,
                        Url = userInfo.Picture.Data.Url

                    };
                    user.Photo = photo;

                    var result = await _userManager.CreateAsync(user);

                    if (!result.Succeeded)
                        throw new RestException(HttpStatusCode.BadRequest, new { User = "Problem creating user" });
                }

                return new UserModel
                {
                    Username = user.UserName,
                    Token = _jwtGenerator.CreateToken(user),
                    Email = user.Email,
                    Image = user.Photo?.Url
                };


            }
        }

    }
}