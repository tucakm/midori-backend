﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistance.Migrations
{
    public partial class PhotoAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PhotoId",
                table: "Services",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Services_PhotoId",
                table: "Services",
                column: "PhotoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Services_Photos_PhotoId",
                table: "Services",
                column: "PhotoId",
                principalTable: "Photos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Services_Photos_PhotoId",
                table: "Services");

            migrationBuilder.DropIndex(
                name: "IX_Services_PhotoId",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "PhotoId",
                table: "Services");
        }
    }
}
