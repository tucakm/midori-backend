﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistance.Migrations
{
    public partial class AddingStatusPhotosAndService : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AppUserId",
                table: "Services",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Services_AppUserId",
                table: "Services",
                column: "AppUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Services_AspNetUsers_AppUserId",
                table: "Services",
                column: "AppUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Services_AspNetUsers_AppUserId",
                table: "Services");

            migrationBuilder.DropIndex(
                name: "IX_Services_AppUserId",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "AppUserId",
                table: "Services");
        }
    }
}
