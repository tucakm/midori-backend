﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistance.Migrations
{
    public partial class NotificationsColumChange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Notifications_Services_OrderdServiceId",
                table: "Notifications");

            migrationBuilder.DropIndex(
                name: "IX_Notifications_OrderdServiceId",
                table: "Notifications");

            migrationBuilder.DropColumn(
                name: "OrderdServiceId",
                table: "Notifications");

            migrationBuilder.CreateIndex(
                name: "IX_Notifications_OrderedServiceId",
                table: "Notifications",
                column: "OrderedServiceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Notifications_Services_OrderedServiceId",
                table: "Notifications",
                column: "OrderedServiceId",
                principalTable: "Services",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Notifications_Services_OrderedServiceId",
                table: "Notifications");

            migrationBuilder.DropIndex(
                name: "IX_Notifications_OrderedServiceId",
                table: "Notifications");

            migrationBuilder.AddColumn<Guid>(
                name: "OrderdServiceId",
                table: "Notifications",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Notifications_OrderdServiceId",
                table: "Notifications",
                column: "OrderdServiceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Notifications_Services_OrderdServiceId",
                table: "Notifications",
                column: "OrderdServiceId",
                principalTable: "Services",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
