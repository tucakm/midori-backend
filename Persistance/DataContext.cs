using Domain;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Persistance
{
    public class DataContext : IdentityDbContext<AppUser>
    {

        public DataContext(DbContextOptions options) : base(options)
        {
        }
        public DbSet<Service> Services { get; set; }

        public DbSet<Photo> Photos { get; set; }

        public DbSet<Review> Reviews { get; set; }

        public DbSet<Tag> Tag { get; set; }

        public DbSet<ServiceTags> ServiceTags { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<Notification> Notifications { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Service>().Property(b => b.Status).HasDefaultValue(ServiceStatus.Active);

            builder.Entity<Service>()
                .HasOne(s => s.User)
                .WithMany(ap => ap.Service)
                .HasForeignKey(s => s.AppUserId);


            builder.Entity<ServiceTags>(x => x.HasKey(t =>
               new { t.TagId, t.ServiceId }));

            builder.Entity<ServiceTags>()
                .HasOne(u => u.Tag)
                .WithMany(a => a.ServiceTags)
                .HasForeignKey(u => u.TagId);


        }

    }
}