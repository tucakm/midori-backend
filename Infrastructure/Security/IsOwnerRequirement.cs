using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Persistance;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Infrastructure.Security
{
    public class IsOwnerRequirement : IAuthorizationRequirement
    {

    }
    public class IsOwnerRequirementHandler : AuthorizationHandler<IsOwnerRequirement>
    {

        private readonly DataContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public IsOwnerRequirementHandler(IHttpContextAccessor httpContextAccessor, DataContext context)
        {
            this._httpContextAccessor = httpContextAccessor;
            this._context = context;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, IsOwnerRequirement requirement)
        {
            var currentUserName = _httpContextAccessor.HttpContext.User?.Claims.SingleOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;

            var serviceId = Guid.Parse(_httpContextAccessor.HttpContext.Request.RouteValues.SingleOrDefault(x => x.Key == "id").Value.ToString());


            var service = _context.Services.FindAsync(serviceId).Result;

            var owner = service?.User;

            if (owner?.UserName == currentUserName)
                context.Succeed(requirement);

            return Task.CompletedTask;

        }
    }
}