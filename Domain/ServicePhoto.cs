﻿using System;

namespace Domain
{
    public class ServicePhoto
    {
        public Guid PhotoId { get; set; }
        public virtual Photo Photo { get; set; }
        public Guid ServiceId { get; set; }
        public virtual Service Service { get; set; }
    }
}
