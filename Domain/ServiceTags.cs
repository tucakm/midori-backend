using System;

namespace Domain
{
    public class ServiceTags
    {
        public Guid TagId { get; set; }
        public virtual Tag Tag { get; set; }
        public Guid ServiceId { get; set; }
        public virtual Service Service { get; set; }

    }
}