using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace Domain
{
    public class AppUser : IdentityUser
    {
        /*  public AppUser()
          {
              Photos = new Collection<Photo >();
          } */
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string Bio { get; set; }
        public string Job { get; set; }
        public string Website { get; set; }
        public string City { get; set; }
        public string Company { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public string AboutMe { get; set; }
        public DateTime Birthday { get; set; }
        public virtual ICollection<Service> Service { get; set; }
        public string PhotoId { get; set; }
        public virtual Photo Photo { get; set; }
        public string FaceBookLink { get; set; }
        public string TwitterLink { get; set; }
        public string InstagramLink { get; set; }


    }
}