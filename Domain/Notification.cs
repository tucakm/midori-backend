﻿using System;

namespace Domain
{
    public class Notification
    {
        public Guid Id { get; set; }

        public Guid OrderedServiceId { get; set; }
        public virtual Service OrderedService { get; set; }

        public Guid? OrderId { get; set; }

        public virtual Order Order { get; set; }
        public string AppUserId { get; set; }
        public virtual AppUser User { get; set; }

        public string Message { get; set; }

        public DateTime Date { get; set; }

        public bool IsRead { get; set; }

        public NotificationType Type { get; set; }
    }

    public enum NotificationType
    {
        NewOrder = 1,
        RescheduleOrder = 2,
        Review = 3,

    }
}
