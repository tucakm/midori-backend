using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public class Service
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Introduction { get; set; }
        public decimal Price { get; set; }
        public DateTime CreatedDate { get; set; }
        public string AppUserId { get; set; }
        public virtual AppUser User { get; set; }
        public virtual ICollection<Review> Review { get; set; }
        public virtual ICollection<ServiceTags> ServiceTags { get; set; }
        //public virtual ICollection<Photo> Photos { get; set; }
        public string PhotoId { get; set; }
        public virtual Photo Photo { get; set; }
        public ServiceStatus Status { get; set; }
        public string City { get; set; }

    }

    public enum ServiceStatus
    {
        Draft = 1,
        Active = 2,
        Inactive = 3,
    }
}