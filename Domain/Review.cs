using System;

namespace Domain
{
    public class Review
    {
        public Guid Id { get; set; }
        public virtual Service Service { get; set; }
        public virtual AppUser User { get; set; }
        public string Text { get; set; }
        public int Rate { get; set; }
        public DateTime Date { get; set; }
    }
}