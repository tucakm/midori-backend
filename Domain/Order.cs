﻿using System;

namespace Domain
{
    public class Order
    {
        public Guid Id { get; set; }
        public Guid ServiceId { get; set; }
        public virtual Service Service { get; set; }
        public string AppUserId { get; set; }
        public virtual AppUser User { get; set; }
        public DateTime Date { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public OrderStatus Status { get; set; }
        public decimal TotalPrice { get; set; }
        public string Comment { get; set; }


    }

    public enum OrderStatus
    {
        Pending = 1,
        Aproved = 2,
        Declined = 3,
        Finished = 4,
        Reschedule = 5,
        RescheduleSent = 6,

    }
}
